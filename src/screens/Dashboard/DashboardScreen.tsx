import React, { useEffect } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { loginRequest } from '../../redux/actions/AuthAction';
import { ReducerStateIF } from '../../redux/reducers';
import DashboardAddPost from '../../components/Dashboard/DashboardAddPost';
import DashboardMeetMembers from '../../components/Dashboard/DashboardMeetMembers';
import DashboardEvent from '../../components/Dashboard/DashboardEvent';
import DashboardPost from '../../components/Dashboard/DashboardPost';
import DashboardUserDerail from '../../components/Dashboard/DashboardUserDetail';
import DashboardSuggestion from '../../components/Dashboard/DashboardSuggestion';
import DashboardTopic from '../../components/Dashboard/DashboardTopic';
import DashboardChatPopup from '../../components/Dashboard/DashboardChatPopup';
import './DashboardStyle.scss';
interface Props {
    loader: boolean;
    doLogin: typeof loginRequest;
}

const DashboardScreen: React.FC<Props> = ({ loader, doLogin }) => {
    useEffect(() => {
        setTimeout(doLogin, 2000);
    }, [doLogin]);

    // const validateUsername = (username: string | null) => {
    //     if(username === "Admin"){

    //     }

    //     else if(username !== null) {

    //     }
    // }

    return (
        <div className="row page-content content-page">
            <div className="col-lg-8 order-sm-2 order-md-2 order-xs-2 order-lg-1">
                <DashboardAddPost />
                <DashboardMeetMembers />
                <DashboardEvent />
                <DashboardPost />
            </div>
            <div className="col-lg-4 order-sm-1 order-md-1 order-xs-1 order-lg-2">
                <DashboardUserDerail />
                <DashboardSuggestion />
                <DashboardTopic />        
                <DashboardChatPopup />
            </div>
        </div>
    );
}
 
const mapStateToProps = ({ app }: ReducerStateIF) => ({
    loader: app.loader
});

const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators({
        doLogin: loginRequest
    }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen);
