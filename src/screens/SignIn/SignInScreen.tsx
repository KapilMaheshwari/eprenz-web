import { Link } from "react-router-dom";
import skyImage from '../../assets/images/Others/sky-image.jpg';
import yellowBackground from '../../assets/images/illustrations/yellowBackground.svg';
import purpleBackground from '../../assets/images/illustrations/purpleBackground.svg';
const SignInScreen = (props: any) => {

        return(
            <>
            <section className="sign-in-page">
                <img alt="yellowImage" src={yellowBackground} className="img-fluid yellow-background"/>
                <img alt="purpleImage" src={purpleBackground} className="img-fluid purple-background purple-background-1"/>
                <div className=" p-0">
                    <div className="row no-gutters">
                        <div className="col-lg-6 col-md-6 sign-in-form">
                            <div className="row justify-content-center">
                                <div className="col-lg-8 col-md-8">
                                    <div className="main-step-form signup-login-form ak-signin-main">
                                        <div className="text-center ak-welcome signup-head">
                                            <h3>Welcome Back !</h3>
                                        </div>
                                        <form>
                                            <div className="row">
                                                <div className="col-lg-12">
                                                    <div className="form-group">
                                                        <label>Username Or Email</label>
                                                        <input type="text" className="form-control" placeholder="Enter Username or Email"/>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12">
                                                    <div className="form-group mb-0">
                                                        <label>Password</label>
                                                        <input type="text" className="form-control" placeholder="Enter Password"/>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 text-right mb-4">
                                                    <Link to="/forgot" className="forgot-password">Forgot Password?</Link>
                                                </div>
                                                <div className="col-lg-12">
                                                    <Link to="/" className="create-account-btn">LOGIN</Link>
                                                </div>
                                                <div className="col-lg-12 text-center my-3">
                                                    <span className="or">OR</span>
                                                </div>
                                                <div className="col-lg-12">
                                                    <button type="button" className="create-account-btn signup-linkedin">LOGIN With Linkedin</button>
                                                </div>
                                                <div className="col-lg-12 mt-4 text-center signup-head">
                                                    <span>Don't have an account? <Link to="/sign-up">SIGNUP HERE</Link> </span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div className="ak-section-image">
                                <img alt="skyImage" src={skyImage} className="img-fluid skyimage skyimage1"/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
             </>
        );
}
export default SignInScreen;