import React, { Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Header from '../components/layout/Header/Header';
import Sidebar from '../components/layout/Sidebar/Sidebar'; 
import ClubsScreen from "../screens/Clubs/ClubsScreen";
import CompaniesScreen from "../screens/Companies/CompaniesScreen";
import DashboardScreen from '../screens/Dashboard/DashboardScreen';
import DealsScreen from "../screens/Deals/DealsScreen";
import DiscoveryScreen from '../screens/Discovery/DiscoveryScreen';
import EventsScreen from '../screens/Events/EventsScreen';
import MembersScreen from '../screens/Members/MembersScreen';
import TopicsScreen from "../screens/Topics/TopicsScreen";


const AppMain: React.FC<{}> = () => {
    return (
        <>
            <Suspense fallback={<span>loading</span>}>
                <Header />
                <Sidebar />
                <Switch>
                    <Route exact path="/" component={DashboardScreen} />
                    <Route exact path="/discovery" component={DiscoveryScreen} />
                    <Route exact path="/events" component={EventsScreen} />
                    <Route exact path="/members" component={MembersScreen} />
                    <Route exact path="/clubs" component={ClubsScreen} />
                    <Route exact path="/companies" component={CompaniesScreen} />
                    <Route exact path="/deals" component={DealsScreen} />
                    <Route exact path="/topics" component={TopicsScreen} />
                </Switch>
            </Suspense>
        </>
    );
};

export default AppMain;
