import React from "react";
import styled from "@emotion/styled";

const DarkButton = ({ text } : {text:any}) => (
    <Button className="btn">
      {text}
    </Button>
);

const Button = styled.button`
    text-decoration: none;
    display: inline-block;
    background: #f4f7f7;
    color: #555;
    font-size: 14px;
    padding: 10px 20px;
    transition: background 500ms ease-in-out;
    border-radius: 50px;
    font-weight: 600;
    letter-spacing: 0.5px;
    border: 1px solid #f4f7f7;
    &:hover {
        background: transparent;
        color: #555;
    }
`;

export default DarkButton;