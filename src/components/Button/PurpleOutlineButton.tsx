import React from "react";
import styled from "@emotion/styled";

const PurpleOutlineButton = ({ text } : {text:any}) => (
    <Button className="btn">
      {text}
    </Button>
);

const Button = styled.button`
    text-decoration: none;
    display: inline-block;
    background: transparent;
    color: #3e2d8d;
    font-size: 14px;
    padding: 10px 20px;
    transition: background 500ms ease-in-out;
    border-radius: 50px;
    font-weight: 600;
    letter-spacing: 0.5px;
    border: 1px solid #3e2d8d;
    &:hover {
        background: #3e2d8d;
        color: #fff;
    }
`;

export default PurpleOutlineButton;