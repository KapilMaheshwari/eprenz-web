import React from "react";
import styled from "@emotion/styled";

const TopicButton = ({ text } : {text:any}) => (
    <Button className="btn">
      {text}
    </Button>
);

const Button = styled.button`
    text-decoration: none;
    display: inline-block;
    background: transparent;
    color: #25C105;
    font-size: 14px;
    padding: 10px 20px;
    transition: background 500ms ease-in-out;
    border-radius: 8px;
    font-weight: 600;
    letter-spacing: 0.5px;
    border: 1px solid #25C105;
    &:hover {
        background: #25C105;
        color: #fff;
    }
`;

export default TopicButton;