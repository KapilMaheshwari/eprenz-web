import React from "react";
import { Link } from "react-router-dom";
import Row from "../Row/Row";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import user6 from './../../assets/images/user/user1.jpg';
import './DashboardStyle.scss';

const DashboardUserDerail: React.FC = () =>{
    return(
        <div className="Userdetail">
            <Row>
                <div className="col-lg-5 col-md-5 col-sm-5 pl-lg-0">
                    <Card>
                        <CardBody>
                            <div className="own-user-profile">
                                <img src={user6} />
                                <h4>Donne Hicks</h4>
                                <h6>ADMIN</h6>
                                <p>Finance &amp; Admin</p>
                            </div>
                        </CardBody>
                    </Card>
                </div>
                <div className="col-lg-7 col-md-7 col-sm-7 pr-lg-0">
                    <Card>
                        <CardBody>
                            <div className="own-user-description">
                                <h4>Chief Technology Officer & Founding Team</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500</p>
                                <Link to="#" className="blue-btn">
                                View Profile</Link>
                            </div>
                        </CardBody>
                    </Card>
                </div>
            </Row>
        </div>
    );
};

export default DashboardUserDerail;