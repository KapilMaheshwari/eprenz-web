import React from "react";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import Row from "../Row/Row";
import SuggestedTopic from "../TopicList/SuggestedTopic";
import topic from "../../assets/images/topic/GlobalHealth.jpg";
import { Link } from 'react-router-dom';

import './DashboardStyle.scss';

const DashboardTopic: React.FC = () =>{

    const data = [
        {
            id:1,
            topicName:'Start a Business',
            topicImage:topic
        },
        {
            id:2,
            topicName:'Start a Business1',
            topicImage:topic
        },
        {
            id:3,
            topicName:'Start a Business2',
            topicImage:topic
        }
    ]

    return(
            <Card>
                <CardBody>
                    <div className="topic-header">
                        <h4>Topics</h4>
                        <Link to="#">See All</Link>
                    </div>
                    <Row>
                        {data.map((d:any,i:any) => (
                            <SuggestedTopic data={d} key={i}/>
                        ))}
                    </Row>
                </CardBody>
            </Card>
    );
};

export default DashboardTopic;