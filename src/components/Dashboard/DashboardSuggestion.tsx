import React from "react";
import MembersList from "../MemberList/MemberList";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import Row from "../Row/Row";
import './DashboardStyle.scss';

const DashboardSuggestion: React.FC = () =>{


    const data = [
        {
            id:1,
            name:'Vaibhav',
            location:'surat, Gujarat',
            company:'Tech Solution'
        },
        {
            id:2,
            name:'Akash',
            location:'surat, Gujarat',
            company:'Tech Solution'
        },
        {
            id:3,
            name:'Jignesh',
            location:'surat, Gujarat',
            company:'Tech Solution'
        }
    ]

    return(
        <>
            <div className="Suggestion">
                <Card>
                    <CardBody>
                        <div className="topic-header">
                            <h4>Suggestion</h4>
                            <a>See All</a>
                        </div>
                        <Row>
                            {data.map((d: any,i: any) => {
                                return (
                                    <div className="col-lg-6 col-sm-4 col-md-4 col-xs-6" key={i} >
                                        <MembersList data={d} />
                                    </div>
                                )
                            })}
                        </Row>
                    </CardBody>
                </Card>
            </div>
        </>
    );
};

export default DashboardSuggestion;