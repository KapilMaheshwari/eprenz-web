import React from "react";
import EventList from "./../../components/EventList/EventList";
import Row from '../Row/Row';

const DashboardEvent: React.FC = (children) =>{

    const data = [
        {
            id:1,
            month:'Apr',
            date:'22',
            title:'PeerConnect',
            subTitle:'saj',
            time:'12 : 00 AM'
        },
        {
            id:2,
            month:'May',
            date:'14',
            title:'PeerConnect1',
            subTitle:'saj1',
            time:'10 : 00 AM'
        },
        {
            id:3,
            month:'June',
            date:'12',
            title:'PeerConnect2',
            subTitle:'saj2',
            time:'09 : 00 AM'
        },
    ]

    return(
        <>
            <Row>
                <div className="col-12 event-head">
                    <h4 className="title_head">Upcoming events</h4>
                </div>
                {data.map((d: any,i: any) => (
                    <div className="col-lg-4 col-md-4 col-sm-6 col-xs-12 mb-3" key={i}>
                        <EventList data={d} />
                    </div>
                ))}
            </Row>
        </>
    );
};

export default DashboardEvent;