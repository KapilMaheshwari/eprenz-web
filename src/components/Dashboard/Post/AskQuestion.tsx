import { Link } from 'react-router-dom';
import user from '../../../assets/images/user/user1.jpg';

export interface Props {
    active: any;
}

const AskQuestion = ({active}: Props) => {
    return (
        <div className={`tab-pane fade ${active === 1 && 'show active'}`} id="add-question" role="tabpanel" aria-labelledby="add-question-tab">
            <div className="row">
                <span className="selected_topic">#GOOD HEALTH & WELLNESS</span>
                <span className="selected_topic">#MEDITATION</span>
                <Link to="#" className="add-topic-btn" data-toggle="modal" data-target="#exampleModal">
                    <span>+</span>ADD TOPICS
                </Link>
            </div>
            <div className="add_post_textarea add_post row">
                <div className="col-lg-1">
                    <img src={user} alt="userimg" className="Postuserimg"/>
                </div>
                <div className="col-lg-11">
                    <textarea className="form-control Posttextarea" placeholder="Share what's on your mind..." ></textarea>
                </div>
            </div>
            <input type="text" className="form-control question_poll mt-2" placeholder="Add Questions" />
            <input type="text" className="form-control question_poll mt-2" placeholder="Poll Answer 1" />
            <div className="input-group row mt-2">
                <input type="text" className="form-control question_poll col-lg-11" placeholder="Poll Answer 2"/>
                <div className="input-group-append col-lg-1">
                    <button className="btn" type="button">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="21" height="21" viewBox="0 0 21 21">
                        <image id="Vector_Smart_Object_copy_20" data-name="Vector Smart Object copy 20" width="21" height="21" xlinkHref="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAABzklEQVQ4jbWVPSxEQRSFP89SKhAtCY2sxCYkEhKrUKlWp0M0olKQ0CklChWdoNP5q3S7EQoVhW4lEtUmG4UIsVuQ+5yRydhdP+EkLzPzZu5599y59746eoapgjQwobELaADKQB7IAXsaP6ESaS+wCQxW+5qHc2AOuPJfRsGhSeBChA/AOmBfbYkdeB+H9f5B5y5k9wHfU9vY0dzGRaBYw8tWYA2Y0trGXZvU09buJB/Fa1gW4dMX0m3/EHgBRoEx4BgoOPkWw0Z5uPqNWPpYlV2jeGL5drtZxaiziuSMngM9lUJxAzQBI5HSxrBVI4YpxTxVZb8oe8NEpDw07P9Qdghnn04osQ3XwaGM59mIN65ofhmEwtl3WUxftagLSHfC/Auw66WTQ8yVUOlZCTYD994B8+LW8zCtssx6nvpo1rycUC13A0ngNCB18lZEmvXkh0hqnY+8pjBeQ+p34OxzkbqNYUb5VgmXimEo2aFV9oY9V/tnag52OdO/8HJbl2Zda8iVqbWvkjaWfki4JLuSeD4aSgG4U25ac+iQ97WaikneABa0NvknPqnBGq2lkHWbPmAWaBPxI/CstOkH5hWqAXlohHHbM/xL5//7fxTwBn8RbJqaAB3MAAAAAElFTkSuQmCC"/>
                        </svg>
                    </button>
                </div>
            </div>
            <div className="text-right mt-3">
                <button type="button" className="add-post">
                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="22" viewBox="0 0 21 22">
                        <path id="Path" className="cls-1" d="M3,10.281L18,2.7l-7.105,16L9.316,11.965Z"/>
                    </svg>
                    Post
                </button>
            </div>
        </div>
    )
}

export default AskQuestion;
