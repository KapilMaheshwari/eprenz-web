import user from '../../../assets/images/user/user1.jpg';
import { Link } from "react-router-dom";

export interface Props {
    active: any;
    setShowModal: any;
}

const AddPost = ({
    active, 
    setShowModal
}: Props) => {

    return (
        <div className={`tab-pane fade  ${active === 0 && 'show active'}`} id="add-post" role="tabpanel" aria-labelledby="add-post-tab">
            <Link to="#" className="add-topic-btn" data-toggle="modal" data-target="#exampleModal">
                <span>+</span>ADD TOPICS
            </Link>
            <div className="add_post_textarea add_post row">
                <div className="col-lg-1">
                    <img src={user} alt="userimg" className="Postuserimg"/>
                </div>
                <div className="col-lg-11" onClick={() => setShowModal(true)}>
                    <textarea data-toggle="modal" data-target="#post-modal" className="form-control Posttextarea" placeholder="Share what's on your mind..." ></textarea>
                </div>
            </div>
            <div className="text-right mt-3">
                <button type="button" className="add-post">
                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="22" viewBox="0 0 21 22">
                        <path id="Path" className="cls-1" d="M3,10.281L18,2.7l-7.105,16L9.316,11.965Z"/>
                    </svg>
                    Post
                </button>
            </div>
        </div>
    )
}

export default AddPost
