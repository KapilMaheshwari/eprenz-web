import user from '../../../assets/images/user/user1.jpg';

const CreatePostModalBody = () => {
    return (
        <div className="modal-body">
                    <div className="d-flex">
                        <div className="user-img">
                            <img src={user} alt="userimg" className="avatar-60 rounded-circle img-fluid"/>
                        </div>
                        <form className="post-text ml-3 w-100" action="javascript:void();">
                            <textarea className="form-control rounded" placeholder="Write something here..."></textarea>
                        </form>
                    </div>
                    <hr/>
                    <div className="img-slider ak-img-slider row">
                    </div>
                    <div className="form-group multi-preview mb-0"></div>
                    <ul className="d-flex flex-wrap align-items-center list-inline m-0 p-0">
                        <li className="col-md-1">
                            <div className="image-upload">
                                <label htmlFor="file-input">
                                    <svg version="1.1" id="Layer_1"  x="0px" y="0px"
                                        width="19px" height="16px" viewBox="0 0 19 16" enableBackground="new 0 0 19 16">
                                        <g><g><path fill="none" stroke="#819BA2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" d="
                                                    M18,13.44C18,14.3,17.31,15,16.45,15H2.55C1.69,15,1,14.3,1,13.44V4.56C1,3.7,1.69,3,2.55,3h2.59l1.54-2h4.64l1.54,2h3.59
                                                    C17.31,3,18,3.7,18,4.56V13.44z"/>
                                                <path fill="none" stroke="#819BA2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" d="
                                                    M9,5.82c1.71,0,3.09,1.4,3.09,3.12c0,1.71-1.38,3.11-3.09,3.11s-3.09-1.4-3.09-3.11C5.91,7.22,7.29,5.82,9,5.82z"/></g></g>
                                    </svg>
                                </label>
                                {/* <input id="file-input" type="file" multiple onChange={this.uploadMultipleFiles} accept="image/*"  /> */}
                            </div>
                        </li>
                        <li className="col-md-1">
                            <div className="image-upload">
                                <label htmlFor="file-video">
                                    <svg version="1.1" id="Layer_1" x="0px" y="0px"
                                        width="20px" height="14px" viewBox="0 0 20 14" enableBackground="new 0 0 20 14" >
                                        <g><g><g><path fill="#819BA2" d="M19,12.46c-0.212,0-0.423-0.067-0.599-0.2l-5.73-4.29c-0.252-0.189-0.401-0.486-0.401-0.801
                                                        c0-0.315,0.149-0.612,0.401-0.801l5.73-4.28c0.304-0.226,0.708-0.262,1.047-0.093C19.787,2.166,20,2.512,20,2.89v8.57
                                                        c0,0.378-0.214,0.725-0.552,0.894C19.306,12.425,19.153,12.46,19,12.46z M14.94,7.171L18,9.462V4.885L14.94,7.171z"/>
                                                </g><g><path fill="#819BA2" d="M11.36,14H2.64C1.185,14,0,12.78,0,11.28V2.72C0,1.22,1.185,0,2.64,0h8.72C12.791,0,14,1.324,14,2.89
                                                        v8.39C14,12.78,12.815,14,11.36,14z M2.64,2C2.287,2,2,2.323,2,2.72v8.56C2,11.677,2.287,12,2.64,12h8.72
                                                        c0.353,0,0.64-0.323,0.64-0.72V2.89C12,2.419,11.642,2,11.36,2H2.64z"/></g></g></g>
                                    </svg>
                                </label>
                                <input id="file-video" type="file" accept="video/*"  />
                            </div>
                        </li>
                        <li className="col-md-1">
                            <div className="image-upload">
                                <label>
                                    <svg version="1.1" id="Layer_1"  x="0px" y="0px"
                                        width="17.991px" height="18.015px" viewBox="0 0 17.991 18.015" enableBackground="new 0 0 17.991 18.015" >
                                        <g><g><g><path fill="#819BA2" d="M10.611,12.43c-0.24,0-0.482-0.017-0.724-0.052c-1.329-0.191-2.502-0.888-3.306-1.962
                                                        c-0.331-0.442-0.24-1.069,0.203-1.399c0.443-0.331,1.069-0.24,1.399,0.202c0.997,1.334,2.895,1.607,4.23,0.607
                                                        c0.12-0.09,0.227-0.181,0.333-0.287l2.408-2.417c0.549-0.57,0.85-1.336,0.836-2.145c-0.014-0.808-0.341-1.561-0.921-2.119
                                                        c-1.179-1.141-3.024-1.142-4.201-0.002L9.5,4.224c-0.391,0.391-1.023,0.391-1.414,0c-0.391-0.391-0.391-1.024,0-1.414l1.38-1.38
                                                        c1.969-1.905,5.035-1.905,6.992-0.012c0.964,0.928,1.509,2.181,1.532,3.524c0.023,1.341-0.477,2.612-1.407,3.578l-2.422,2.433
                                                        c-0.177,0.176-0.35,0.325-0.548,0.474C12.733,12.085,11.688,12.43,10.611,12.43z"/></g>
                                                <g><path fill="#819BA2" d="M5.012,18.015c-1.255,0-2.508-0.473-3.485-1.418c-1.987-1.925-2.042-5.111-0.125-7.102l2.422-2.433
                                                        c0.177-0.176,0.35-0.325,0.548-0.474c1.076-0.805,2.4-1.141,3.726-0.951c1.329,0.191,2.502,0.888,3.306,1.962
                                                        c0.331,0.442,0.24,1.069-0.203,1.399c-0.443,0.331-1.069,0.24-1.399-0.202c-0.997-1.333-2.895-1.607-4.23-0.607
                                                        c-0.12,0.09-0.227,0.181-0.333,0.287l-2.408,2.417C1.688,12.081,1.722,14,2.919,15.159c1.177,1.139,3.021,1.139,4.199,0
                                                        l1.356-1.366c0.389-0.392,1.022-0.395,1.414-0.005c0.392,0.389,0.394,1.022,0.005,1.414l-1.37,1.38
                                                        C7.535,17.538,6.273,18.015,5.012,18.015z"/></g></g></g>
                                    </svg>
                                </label>
                            </div>
                        </li>
                        <li className="col-md-1">
                            <div className="image-upload">
                                <label>
                                    <svg version="1.1" id="Layer_1"  x="0px" y="0px"
                                        width="15px" height="18px" viewBox="0 0 15 18" enableBackground="new 0 0 15 18" >
                                        <g><g><path fill="#819BA2" d="M7.5,18c-0.194,0-0.389-0.057-0.557-0.169C6.659,17.64,0,13.109,0,7.55C0,3.387,3.364,0,7.5,0
                                                    S15,3.387,15,7.55c0,5.559-6.659,10.09-6.943,10.281C7.889,17.943,7.694,18,7.5,18z M7.5,2C4.467,2,2,4.49,2,7.55
                                                    c0,3.679,4,7.071,5.499,8.213C8.998,14.619,13,11.218,13,7.55C13,4.49,10.533,2,7.5,2z"/></g><g><path fill="#819BA2" d="M7.5,10.73c-1.748,0-3.17-1.427-3.17-3.18S5.752,4.37,7.5,4.37s3.17,1.427,3.17,3.18
                                                    S9.248,10.73,7.5,10.73z M7.5,6.37c-0.645,0-1.17,0.529-1.17,1.18S6.855,8.73,7.5,8.73s1.17-0.529,1.17-1.18S8.145,6.37,7.5,6.37z
                                                    "/></g></g>
                                    </svg>
                                </label>
                            </div>
                        </li>
                    </ul>
                </div>
    )
}

export default CreatePostModalBody
