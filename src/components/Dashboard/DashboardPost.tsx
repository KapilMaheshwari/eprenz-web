import React from "react";
import PostWithImage from "./../../components/Post/PostWithImage";
import PostWithQuestion from "./../../components/Post/PostWithQuestion";
import PostWithDescription from "./../../components/Post/PostWithDescription";

class DashboardPost extends React.Component{
    render(){
        return(
            <>
                <div className="row">
                    <div className="col-lg-12 mb-3">
                        <PostWithImage />
                    </div>
                    <div className="col-lg-12 mb-3">
                        <PostWithQuestion />
                    </div>
                    <div className="col-lg-12 mb-3">
                        <PostWithDescription />
                    </div>
                </div>
            </>
        );
    }
}

export default DashboardPost;