import React, { useState } from "react";
import './DashboardStyle.scss';
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import Row from "../Row/Row";
import AddPost from './Post/AddPost';
import AskQuestion from "./Post/AskQuestion";
import Modal from '../Modal/ModalComponent';
import CreatePostModalBody from '../Dashboard/Post/CreatePostModalBody';

const DashboardAddPost: React.FC = () => {

    const [active,setActive] = useState(0);
    const [showModal,setShowModal] = useState(false);

    return(
        <>
            <Row>
                <div className="col-lg-12">
                    <Card>
                        <CardBody>
                            <div className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <div className="nav-item" role="presentation" onClick={() => setActive(0)}>
                                    <p className={`nav-link ${active === 0 && 'active'}`} id="add-post-tab" data-toggle="pill" role="tab" aria-controls="add-post" aria-selected="true">Add Post</p>
                                </div>
                                <div className="nav-item" role="presentation" onClick={() => setActive(1)}>
                                    <p className={`nav-link ${active === 1 && 'active'}`} id="add-question-tab" data-toggle="pill" role="tab" aria-controls="add-question" aria-selected="false">Ask Question</p>
                                </div>
                                <div>
                                <i className="fa fa-spinner">no spinner but why</i>
                                </div>
                            </div>
                            <hr/>
                            <div className="tab-content" id="pills-tabContent">
                                {active === 0 && (
                                    <AddPost active={active} setShowModal={setShowModal}/>
                                )}
                                {active === 1 && (
                                    <AskQuestion active={active} />
                                )}
                            </div>
                        </CardBody>
                    </Card>
                </div>
            </Row>
            <Modal showModal={showModal} setShowModal={setShowModal} >
                <CreatePostModalBody />
            </Modal>
        </>
    );
};

export default DashboardAddPost;