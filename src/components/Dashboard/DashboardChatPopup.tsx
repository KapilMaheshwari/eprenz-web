import {useState} from "react";
import { Link } from "react-router-dom";
import user from './../../assets/images/user/user1.jpg';
import user1 from './../../assets/images/user/user2.jpg';
import user2 from './../../assets/images/user/user3.jpg';
import user3 from './../../assets/images/user/user4.jpg';

function DashboardChatPopup(){
        const [show,setShow]=useState(false);
        const [show1,setShow1]=useState(false);
        // const chat_button = {
        //     background: "#3aabef",
        //     border: "0px",
        //     width: "45px",
        //     height: "auto",
        //     borderRadius: "50px",
        //     color: "#fff",
        //     fontSize: "25px",
        //     fontWeight: "600",
        //     textAlign: "center",
        //     cursor: "pointer",
        //     transition: "0.5s all",
        //     position: "fixed",
        //     bottom: "20px",
        //     right: "25px",
        //     boxShadow: "0 0rem 4.5rem rgb(18 38 63 / 20%)",
        // }
        return(
            <>
                <div className="chattoggle-main">
                    {show ? <div className="chatuserlist">
                            <div className="chatuserlist_header">
                                <h5>New Message</h5>
                                <label>To : </label>
                                <input type="Search" ></input>
                            </div>
                            <ul className="chatuserlist_section">
                                <Link to="">
                                    <li>
                                        <img src={user} alt="chat-img"/>
                                        <p>Mathew Firson</p>
                                    </li>
                                </Link>
                                <Link to="">
                                    <li>
                                        <img src={user1} alt="chat-img"/>
                                        <p>John Sandrick</p>
                                    </li>
                                </Link>
                                <Link to="">
                                    <li>
                                        <img src={user2} alt="chat-img"/>
                                        <p>Tara Firson</p>
                                    </li>
                                </Link>
                                <Link to="">
                                    <li>
                                        <img src={user3} alt="chat-img"/>
                                        <p>Kirina Matterson</p>
                                    </li>
                                </Link>
                            </ul>
                        </div>:null
                    }
                </div>
                {/* <div onClick={()=>setShow(!show)} style={chat_button}>
                    <span>+</span>
                </div> */}

                <div className="chattoggle-main chattoggle-main1">
                    {show1 ? <div className="chatuserlist">
                            <div className="chatuserlist_header chatuser-main">
                                <div className="chatuser">
                                    <img alt="userImage" src={user} />
                                    <span className="green-dot"></span>
                                </div>
                                <div>
                                    <h5>John Sandrick</h5>
                                    <p>Active Now</p>
                                </div>
                            </div>
                            <ul className="chatuserlist_section chatuser_chat">
                                <li>
                                    <div className="row">
                                        <div className="col-lg-2">
                                            <img src={user} alt="chat-img"/>
                                        </div>
                                        <div className="col-lg-10">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                            <span>11:00 AM</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-lg-2">
                                            <img src={user} alt="chat-img"/>
                                        </div>
                                        <div className="col-lg-10">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                            <span>11:00 AM</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-lg-2">
                                            <img src={user} alt="chat-img"/>
                                        </div>
                                        <div className="col-lg-10">
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                            <span>11:00 AM</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div className="user-send-message">
                                <input type="search" placeholder="Enter a Message" style={{width: "90%"}}/>
                                <div>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="22" viewBox="0 0 21 22">
                                    <path id="Path" className="cls-1" d="M3,10.281L18,2.7l-7.105,16L9.316,11.965Z"/>
                                    </svg>
                                </div>
                            </div>
                        </div> : null}
                </div>
                {/* <div onClick={()=>setShow1(!show1)} style={chat_button} className="chat-button1">
                    <img src={user} />
                </div> */}
            </>
        );
}

export default DashboardChatPopup;