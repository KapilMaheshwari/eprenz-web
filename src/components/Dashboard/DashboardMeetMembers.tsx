import React from "react";
import user1 from './../../assets/images/user/user1.jpg';
import user2 from './../../assets/images/user/user2.jpg';
import user3 from './../../assets/images/user/user3.jpg';
import user4 from './../../assets/images/user/user4.jpg';
import user5 from './../../assets/images/user/user5.jpg';
import user6 from './../../assets/images/user/user6.jpg';
import user7 from './../../assets/images/user/user7.jpg';
import user8 from './../../assets/images/user/user8.jpg';
import user9 from './../../assets/images/user/user9.jpg';
import user10 from './../../assets/images/user/user10.jpg';
import { Link } from 'react-router-dom';
 
const DashboardMeetMembers: React.FC = () => {
    
    const data = [
        {
            id:1,
            image:user1
        },
        {
            id:2,
            image:user2
        },
        {
            id:3,
            image:user3
        },
        {
            id:4,
            image:user4
        },
        {
            id:5,
            image:user5
        },
        
        {
            id:6,
            image:user6
        },
        
        {
            id:7,
            image:user7
        },
        {
            id:8,
            image:user8
        },
        {
            id:9,
            image:user9
        },
        
        {
            id:10,
            image:user10
        },
        
        {
            id:12,
            image:user1
        },
        
        {
            id:12,
            image:user2
        },
        
        {
            id:13,
            image:user3
        },
        
        {
            id:14,
            image:user4
        },
        {
            id:15,
            image:user5
        },
    ]

    return(
        <>
            <div className="row">
                <div className="col-sm-12 mb-3">
                    <h4 className="title_head mt-3">Meet members in the same profession</h4>
                    <div className="users-thumb-list">
                        {data.map((d: any,i: any) => {
                            if(i < 9) {
                                return (
                                    <Link to='#' >
                                        <img src={d.image} alt="userImage"/>  
                                    </Link>
                                )
                            }
                        }   
                        )}
                        <Link  className="plus-more" to="#" title="" >
                            <div>+{data.length - 10}</div>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    );
};

export default DashboardMeetMembers;