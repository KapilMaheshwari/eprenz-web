import React from "react";
import styled from "@emotion/styled";

const Row = ({ children }: {children:any}) => (
    <Row1>
      {children}
    </Row1>
);

const Row1 = styled.div`
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 0px;
    margin-left: 0px;
    text-align: initial!important;
`;

export default Row;