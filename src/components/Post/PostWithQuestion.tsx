import React from "react";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import TopicButton from "../Button/TopicButton";
import PostComments from "./PostComments";
import PostHeader from "./PostHeader";
import './../../components/Post/Post.css';
import PostFooter from "./PostFooter";
import PostDescription from "./PostDescription";

const PostWithQuestion: React.FC = () =>{
    return(
        <>
            <Card>
                <CardBody>
                    <PostHeader />
                    <PostDescription />
                    <TopicButton text="# START A BUSINESS" />
                    <div className="Pollanswer">
                        <h5>Which Software Is Best ?</h5>
                        <form>
                            {/* <Row>
                                <div className="Polloptions">
                                    <input type="radio" id="test1" name="radio-group"/>
                                    <label>Start a Business</label>
                                    <div className="progress mt-2">
                                        <div className="progress-bar" role="progressbar" style={{width: "50%"}} aria-valuenow="50" aria-valuemin="0" aria-valuetext="50 percent" aria-valuemax="100">20%
                                        </div>
                                    </div>
                                </div>
                                <div className="Polloptions">
                                    <input type="radio" id="test2" name="radio-group"/>
                                    <label>Grow Your Business</label>
                                    <div className="progress mt-2">
                                        <div className="progress-bar" role="progressbar" style={{width: "20%"}} aria-valuenow='50' ariaValuemin="0" ariaValuemax="100">20%
                                        </div>
                                    </div>
                                </div>
                                <div className="Polloptions">
                                    <input type="radio" id="test3" name="radio-group"/>
                                    <label>Global Health & Wellness</label>
                                    <div className="progress mt-2">
                                        <div className="progress-bar" role="progressbar" style={{width: "45%"}} ariaValuenow="50" ariaValuemin="0" ariaValuemax="100">45%</div>
                                    </div>
                                </div>
                                <div className="Polloptions">
                                    <input type="radio" id="test4" name="radio-group"/>
                                    <label>Raise Capital</label>
                                    <div className="progress mt-2">
                                        <div className="progress-bar" role="progressbar" style={{width: "10%"}} ariaValuenow="50" ariaValuemin="0" ariaValuemax="100">10%</div>
                                    </div>
                                </div>
                            </Row> */}
                        </form>
                        <p>831 Votes | <a href="#">Undo</a></p>
                    </div>
                    <PostFooter />
                    <PostComments></PostComments>
                </CardBody>
            </Card>
        </>
    );
};

export default PostWithQuestion;