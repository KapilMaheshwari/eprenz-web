import React from "react";
import Row from "../Row/Row";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import TopicButton from "../Button/TopicButton";
import PostComments from "./PostComments";
import post from "./../../assets/images/user/user1.jpg";
import post1 from "./../../assets/images/user/user2.jpg";
import post2 from "./../../assets/images/user/user3.jpg";
import post3 from "./../../assets/images/user/user4.jpg";
import post4 from "./../../assets/images/user/user5.jpg";
import PostHeader from "./PostHeader";
import PostDescription from "./PostDescription";
import PostFooter from "./PostFooter";

const PostWithImage: React.FC = () =>{
    return(
        <>
            <Card>
                <CardBody>
                    <PostHeader />
                    <PostDescription />
                    <TopicButton text="# START A BUSINESS" />
                    <Row>
                        <div className="col-md-4 col-sm-4 col-xs-6 mt-3">
                            <a href={post} data-fancybox="images" data-caption="Backpackers following a dirt trail">
                                <img alt="post" src={post} className="img-fluid post-images-ak"/>
                            </a>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-6 mt-3">
                            <a href={post1} data-fancybox="images" data-caption="Mallorca, Llubí, Spain">
                                <img alt="post1" src={post1} className="img-fluid post-images-ak"/>
                            </a>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-6 mt-3">
                            <a href={post2} data-fancybox="images" data-caption="Sunrise above a sandy beach">
                                <img alt="post2"src={post2} className="img-fluid post-images-ak"/>
                            </a>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-6 mt-3">
                            <a href={post3} data-fancybox="images" data-caption="Backpackers following a dirt trail">
                                <img alt="post3" src={post3} className="img-fluid post-images-ak"/>
                            </a>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-6 mt-3">
                                <a href={post4} data-fancybox="images" data-caption="Mallorca, Llubí, Spain">
                                    <img alt="post4" src={post4} className="img-fluid post-images-ak"/>
                                </a>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-6 mt-3">
                                <a href={post4} data-fancybox="images" data-caption="Mallorca, Llubí, Spain">
                                    <img alt="post4" src={post4} className="img-fluid post-images-ak"/>
                                </a>
                        </div>
                    </Row>
                    <PostFooter />
                    <PostComments></PostComments>
                </CardBody>
            </Card>
        </>
    );
};

export default PostWithImage;