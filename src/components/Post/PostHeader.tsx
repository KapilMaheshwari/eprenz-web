import React from "react";
import user2 from './../../assets/images/user/user1.jpg';
import './../../components/Post/Post.css';

const PostHeader: React.FC = () =>{
    return(
        <>
            <div className="Postheader">
                <div className="user-detail">
                    <img alt="user2" className="rounded-circle img-fluid" src={user2}  />
                    <div>
                        <a href="#" className="">Barb Ackue</a>
                        <p>02 Apr,2021</p>
                    </div>
                </div>
                <div className="iq-card-post-toolbar">
                    <div className="dropdown">
                        <span className="dropdown-toggle" id="postdata-5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="6" viewBox="0 0 28 6"><path id="Ellipse" className="cls-1" d="M0,3A3,3,0,1,1,3,6,3,3,0,0,1,0,3Z"/><path id="Ellipse-2" data-name="Ellipse" className="cls-1" d="M11,3a3,3,0,1,1,3,3A3,3,0,0,1,11,3Z"/><path id="Ellipse-3" data-name="Ellipse" className="cls-1" d="M22,3a3,3,0,1,1,3,3A3,3,0,0,1,22,3Z"/>
                            </svg>
                        </span>
                        <div className="dropdown-menu m-0 p-0" aria-labelledby="postdata-5">
                            <a className="dropdown-item" href="#">
                                <h6>Report Post</h6>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
};

export default PostHeader;