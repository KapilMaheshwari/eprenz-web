import React from "react";

const PostFooter: React.FC = () =>{
    return(
        <>
            <hr style={{width: "100%"}} />
            <div className="Postfooter">
                <div className="align-items-center like-btn">
                    <a href="#">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            width="17.995px" height="15.998px" viewBox="0 0 17.995 15.998" enableBackground="new 0 0 17.995 15.998" xlinkHref="preserve">
                        <path fill="#FFFFFF" d="M16.608,1.418c-1.85-1.89-4.85-1.89-6.7,0l-0.91,0.93l-0.91-0.93c-1.85-1.89-4.85-1.89-6.7,0
                            c-1.85,1.88-1.85,4.94,0,6.83l0.91,0.93l6.7,6.82l6.7-6.82l0.91-0.93C18.458,6.358,18.458,3.307,16.608,1.418L16.608,1.418z"/>
                        </svg>
                    </a>
                    <span>1 Likes</span>
                </div>
                <div className="align-items-center comment-btn">
                    <a data-toggle="collapse" href="#comment-list-1" role="button" aria-expanded="false">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        width="20px" height="20.21px" viewBox="0 0 20 20.21" enableBackground="new 0 0 20 20.21" xlinkHref="preserve">
                        <g><path fill="none" stroke="#819ba2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" d="
                        M19,9.7c0,1.32-0.3,2.62-0.9,3.8c-1.44,2.88-4.38,4.7-7.6,4.71c-1.32,0-2.62-0.31-3.8-0.91L1,19.21l1.9-5.71
                        C2.3,12.32,2,11.02,2,9.7c0-3.22,1.82-6.36,4.7-7.8C7.88,1.3,9.18,1,10.5,1H11c4.32,0.24,7.76,3.69,8,8V9.7z"/>
                        <polyline fill="none" stroke="#9BACBF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" points="
                        14,8 6,8 6,8.17 	"/>
                        <polyline fill="none" stroke="#9BACBF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" points="
                        14,12 6,12 6,12.17 	"/></g></svg>
                    </a>
                    <span>15 Comments</span>
                </div>
            </div>
        </>
    )
};

export default PostFooter;