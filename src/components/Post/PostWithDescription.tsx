import React from "react";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import TopicButton from "../Button/TopicButton";
import PostComments from "./PostComments";
import PostHeader from "./PostHeader";
import PostDescription from "./PostDescription";
import PostFooter from "./PostFooter";

const PostWithDescription: React.FC = () =>{
    return(
        <>
            <Card>
                <CardBody>
                    <PostHeader />
                    <PostDescription />
                    <PostDescription />
                    <TopicButton text="# START A BUSINESS" />
                    <PostFooter />
                    <PostComments></PostComments>
                </CardBody>
            </Card>
        </>
    );
};

export default PostWithDescription;