import React from "react";
import user from './../../assets/images/user/user1.jpg';
import user4 from './../../assets/images/user/user2.jpg';
import BlueButton from '../Button/BlueButton';
import './../../components/Post/Post.css';

const PostComments: React.FC = () =>{
    return(
        <>
            <div className="Comment">
                <div className="collapse" id="comment-list-1">
                    <hr/>
                    <div className="CommentBody">
                        <div className="media">
                            <img src={user} className="mr-3" alt="Comment User"/>
                            <div className="media-body">
                                <div className="media-user-header">
                                    <h5 className="mt-0">Akash Gajera</h5>
                                    <span>3m Ago</span>
                                </div>
                                <p>Standing on the frontline when the bombs start to fall. Heaven is jealous of our love, angels are crying from up above. Can't replace you with a million rings. Boy, when you're with me I'll give you a taste. There’s no going back. Before you met me I was alright but things were kinda heavy. Heavy is the head that wears the crown.</p>
                                <div className="media-like-comment">
                                    <a>Like . 1</a> &nbsp;|&nbsp; <a>Reply . 1 Replies</a>
                                </div>
                                <div className="media mt-3">
                                    <a className="mr-3" href="#">
                                        <img src={user} alt="Comment User"/>
                                    </a>
                                    <div className="media-body">
                                        <div className="media-user-header">
                                            <h5 className="mt-0">Vanraj Sukhadiya</h5>
                                            <span>3m Ago</span>
                                        </div>
                                        <p>Greetings loved ones let's take a journey. Yes, we make angels cry, raining down on earth from up above. Give you something good to celebrate.</p>
                                        <div className="media-like-comment">
                                            <a>Like . 1</a> &nbsp;|&nbsp; <a>Reply . 1 Replies</a>
                                            <div className="comment-replay-main">
                                                <div className="user-img mr-3 mt-3">
                                                    <img src={user4} alt="userimg" className="avatar-40 rounded-circle"/>
                                                </div>
                                                <form className="comment-text mt-3 post-text w-100">
                                                    <textarea className="form-control rounded" placeholder="Enter Comment"></textarea>
                                                    <button className="replay-btn">REPLAY</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="media mt-3">
                                    <a className="mr-3" href="#">
                                        <img src={user} alt="Comment User"/>
                                    </a>
                                    <div className="media-body">
                                        <div className="media-user-header">
                                            <h5 className="mt-0">Vanraj Sukhadiya</h5>
                                            <span>3m Ago</span>
                                        </div>
                                        <p>Greetings loved ones let's take a journey. Yes, we make angels cry, raining down on earth from up above. Give you something good to celebrate.</p>
                                        <div className="media-like-comment">
                                            <a>Like . 1</a> &nbsp;|&nbsp; <a>Reply . 1 Replies</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="d-flex">
                        <div className="user-img mr-1 mt-3"><img src={user4} alt="userimg" className="avatar-50 rounded-circle"/></div>
                        <form className="comment-text d-flex align-items-center mt-3 post-text ml-3 w-100" action="javascript:void(0);">
                            <textarea className="form-control rounded"></textarea>
                        </form>
                    </div>
                    <div className="PostBtn">
                        <BlueButton text="POST" />
                    </div>
                </div>
            </div>
        </>
    );
};


export default PostComments;