import styled from "@emotion/styled";

const Card = ({ children }: {children:any}) => (
    <Cards>
      {children}
    </Cards>
);

const Cards = styled.div`
    border-radius: 10px;
    margin-botton: 15px;
    background: transparent;
`;

export default Card;