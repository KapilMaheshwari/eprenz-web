import styled from "@emotion/styled";

const CardBody = ({ children }: {children:any}) => (
    <CardsBody>
      {children}
    </CardsBody>
);

const CardsBody = styled.div`
    box-shadow: 0 0.75rem 1.5rem rgb(18 38 63 / 3%);
    background: #fff;
    border-radius: 5px;
    padding: 15px;
`;

export default CardBody;