import React from 'react';
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import Row from "../Row/Row";
import OrangeOutlineButton from "../Button/OrangeOutlineButton";
import { Link } from "react-router-dom";
import './../../components/EventList/EventListStyle.scss';
import event from "./../../assets/images/event/event1.jpg"

export interface Props {
    data:any
};

const EventList = ({
    data
}:Props) => {

    return(
        <>
            <Link to="">
                <Card>
                    <CardBody>
                        <div className="event-main">
                            <img src={event} alt="Peer Connect"/>
                            <Row>
                                <div className="col-lg-4 col-md-4 col-sm-4">
                                    <div className="event-date">
                                        <h3>{data.month}</h3>
                                        <h6>{data.date}</h6>
                                    </div>
                                </div>
                                <div className="col-lg-8 col-md-8 col-sm-8">
                                    <div className="event-title">
                                        <h5>{data.title}</h5>
                                        <h6>{data.subTitle1}</h6>
                                        <div>
                                            <svg xmlnsXlink="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">
                                                <circle id="Path_copy_3" data-name="Path copy 3" className="cls-1" cx="11" cy="10.125" r="8"/>
                                                <path id="Path_copy_3-2" data-name="Path copy 3" className="cls-2" d="M10,6.314v4.8l3.2,1.6h0"/>
                                            </svg>
                                            {data.time}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-12 d-flex justify-content-between">
                                    <OrangeOutlineButton text="Interested" />
                                    <div className="star-main">
                                        <svg xmlnsXlink="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
                                            <path id="Path" className="cls-1" d="M11,0.019l3.393,7.23,7.588,1.158-5.49,5.627,1.3,7.946L11,18.228,4.214,21.981l1.3-7.946L0.019,8.407,7.607,7.249Z"/>
                                        </svg>
                                    </div>
                                </div>
                            </Row>
                        </div>
                    </CardBody>
                </Card>
            </Link>
        </>
    );
};

export default EventList;