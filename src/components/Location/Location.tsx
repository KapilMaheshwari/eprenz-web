import styled from "@emotion/styled";

const Location = () =>(
    <Locations>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="15px" height="18px" viewBox="0 0 15 18" enableBackground="new 0 0 15 18" xlinkHref="preserve">
        <g><g><path fill="#555" d="M7.5,18c-0.194,0-0.389-0.057-0.557-0.169C6.659,17.64,0,13.109,0,7.55C0,3.387,3.364,0,7.5,0
                    S15,3.387,15,7.55c0,5.559-6.659,10.09-6.943,10.281C7.889,17.943,7.694,18,7.5,18z M7.5,2C4.467,2,2,4.49,2,7.55
                    c0,3.679,4,7.071,5.499,8.213C8.998,14.619,13,11.218,13,7.55C13,4.49,10.533,2,7.5,2z"/>
            </g><g><path fill="#555" d="M7.5,10.73c-1.748,0-3.17-1.427-3.17-3.18S5.752,4.37,7.5,4.37s3.17,1.427,3.17,3.18
                    S9.248,10.73,7.5,10.73z M7.5,6.37c-0.645,0-1.17,0.529-1.17,1.18S6.855,8.73,7.5,8.73s1.17-0.529,1.17-1.18S8.145,6.37,7.5,6.37z
                    "/></g></g></svg>
    </Locations>
);

const Locations= styled.span`
    svg{
        color: #555;
        margin-right: 5px;
        width: 14px;
        font-weight: 600;
    }
`;

export default Location;