import styled from "@emotion/styled";
import CardBody from "../Card/CardBody";
import { Link } from 'react-router-dom';

export interface Props {
    data: any
}

const SuggestedTopic = ({data}: Props) =>{
    return( 
        <>
            <Suggested className="col-lg-12">
                <img src={data.topicImage} alt="suggest topic"/>
                <CardBody>
                    <TopicMain>
                        <TopicContent>
                            <h5>{data.topicName}</h5>
                            <Link to="#" >READ MORE...</Link>
                        </TopicContent>
                    </TopicMain>
                </CardBody>
            </Suggested>
        </>
    );
};

const Suggested = styled.div`
    margin-bottom: 10px;
    img{
        width: 100%;
        height: 150px;
        object-fit: cover;
        border-radius: 7px 7px 0px 0px;
    }
`;

const TopicMain = styled.div`
    padding-top: 40px;
    position: relative;
`;

const TopicContent = styled.div`
    position: absolute;
    background: #fff;
    width: 100%;
    box-shadow: 0 0.75rem 1.5rem rgb(0 113 255 / 5%);
    text-align: center;
    top: -60px;
    padding: 15px;
    border-radius: 8px;
    h5{
        font-size: 18px;
        font-weight: 600;
        letter-spacing: 0.5px;
        color: #002c3e;
        height: 25px;
        overflow: hidden;
        margin-bottom: 8px;
    }
    a{
        font-size: 18px;
        text-transform: uppercase;
        color: #ff5900;
        font-weight: 800;
        letter-spacing: 0.5px;
        text-decoration: underline;
    }
`;

export default SuggestedTopic;