import React from "react";
import {Switch , Route} from "react-router-dom";
import Header from "./Header/Header";
import Sidebar from "./Sidebar/Sidebar";
import DashboardScreen from "../../screens/Dashboard/DashboardScreen";

const Layout: React.FC = () => {
    return(
        <div>
            <Header />
            <div id="content-page" className="content-page">
                <Sidebar />
                <Switch>
                    <Route exact={true} path="/" component={DashboardScreen} />
                </Switch>
            </div>
        </div>
    ); 
}

export default Layout;