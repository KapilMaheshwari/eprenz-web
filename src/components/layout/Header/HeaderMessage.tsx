import React from "react";
import { Link } from "react-router-dom";
import user1 from './../../../assets/images/user/user1.jpg';

const HeaderMessage: React.FC = () => {
    return(
        <>
            <li>
                <a href="#" className="search-toggle pr-0 ak-header-chat-icon d-flex align-items-center">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    width="20px" height="20.21px" viewBox="0 0 20 20.21" enableBackground="new 0 0 20 20.21" xlinkHref="preserve">
                    <g>
                        <path fill="none" stroke="#555" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" d="
                            M19,9.7c0,1.32-0.3,2.62-0.9,3.8c-1.44,2.88-4.38,4.7-7.6,4.71c-1.32,0-2.62-0.31-3.8-0.91L1,19.21l1.9-5.71
                            C2.3,12.32,2,11.02,2,9.7c0-3.22,1.82-6.36,4.7-7.8C7.88,1.3,9.18,1,10.5,1H11c4.32,0.24,7.76,3.69,8,8V9.7z"/>
                            <polyline fill="none" stroke="#555" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" points="
                            14,8 6,8 6,8.17 	"/>
                            <polyline fill="none" stroke="#555" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" points="
                            14,12 6,12 6,12.17 	"/>
                    </g>
                </svg>
                </a>
            <div className="iq-sub-dropdown">
                <div className="iq-card shadow-none m-0">
                    <div className="iq-card-body p-0 ak-notification-main">
                        <div className="py-2 px-3">
                            <div className="row header-popup-heading"> 
                                <p>Messages</p>
                                <Link to="">See All</Link>
                            </div>
                        </div>
                        <hr className="m-0 p-0" />
                        <div className="ak-notification-content">
                            <a href="#">
                                <div className="media ak-media-main">
                                    <img src={user1} className="mr-3" alt="..."/>
                                    <div className="media-body">
                                        <div className="d-flex justify-content-between">
                                            <h6 className="mt-0">Akash Gajera</h6>
                                            <p>11:33</p>
                                        </div>
                                        <p>Will you do the same for me?</p>
                                    </div>
                                </div>
                            </a>
                            <a href="#">
                                <div className="media ak-media-main">
                                    <img src={user1} className="mr-3" alt="..."/>
                                    <div className="media-body">
                                        <div className="d-flex justify-content-between">
                                            <h6 className="mt-0">Akash Gajera</h6>
                                            <p>11:33</p>
                                        </div>
                                        <p>Will you do the same for me?</p>
                                    </div>
                                </div>
                            </a>
                            <a href="#">
                                <div className="media ak-media-main">
                                    <img src={user1} className="mr-3" alt="..."/>
                                    <div className="media-body">
                                        <div className="d-flex justify-content-between">
                                            <h6 className="mt-0">Akash Gajera</h6>
                                            <p>11:33</p>
                                        </div>
                                        <p>Will you do the same for me?</p>
                                    </div>
                                </div>
                            </a>
                            <a href="#">
                                <div className="media ak-media-main">
                                    <img src={user1} className="mr-3" alt="..."/>
                                    <div className="media-body">
                                        <div className="d-flex justify-content-between">
                                            <h6 className="mt-0">Akash Gajera</h6>
                                            <p>11:33</p>
                                        </div>
                                        <p>Will you do the same for me?</p>
                                    </div>
                                </div>
                            </a>
                            <a href="#">
                                <div className="media ak-media-main">
                                    <img src={user1} className="mr-3" alt="..."/>
                                    <div className="media-body">
                                        <div className="d-flex justify-content-between">
                                            <h6 className="mt-0">Akash Gajera</h6>
                                            <p>11:33</p>
                                        </div>
                                        <p>Will you do the same for me?</p>
                                    </div>
                                </div>
                            </a>
                            <a href="#">
                                <div className="media ak-media-main">
                                    <img src={user1} className="mr-3" alt="..."/>
                                    <div className="media-body">
                                        <div className="d-flex justify-content-between">
                                            <h6 className="mt-0">Akash Gajera</h6>
                                            <p>11:33</p>
                                        </div>
                                        <p>Will you do the same for me?</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                </div>
        </li>
        </>
    );
}

export default HeaderMessage;