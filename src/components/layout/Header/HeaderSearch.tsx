import React from "react";

const HeaderSearch: React.FC = () => {
    return(
        <div className="iq-search-bar ak-search ak-search-12">
            <form action="#" className="searchbox">
                <input type="text" className="text search-input" placeholder="Search for people business community..." />
                <a className="search-link" href="#">
                    <i className="fa fa-search"></i>
                </a>
            </form>
        </div>
    )
};

export default HeaderSearch;