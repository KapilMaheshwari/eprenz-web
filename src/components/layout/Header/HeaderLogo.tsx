import React from "react";
import logo from './../../../assets/images/others/Logo.png';
import { Link } from "react-router-dom";

const HeaderLogo: React.FC = () => {
    return(
        <div className="iq-navbar-logo d-flex justify-content-between">
            <Link to="/">
                <img src={logo} className="img-fluid" alt="Eprenz-Logo"/>
            </Link>
            <div className="iq-menu-bt align-self-center">
                <div className="wrapper-menu">
                {/* <div className="main-circle">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 50 50" width="50px" height="50px">
                        <g id="surface4820847">
                        <path d="M 172.005404 25.801622 L 172.005404 42.997296 L -0.00540742 42.997296 L -0.00540742 25.801622 Z M 172.005404 77.402162 L 172.005404 94.597835 L -0.00540742 94.597835 L -0.00540742 77.402162 Z M 172.005404 129.002701 L 172.005404 146.198375 L -0.00540742 146.198375 L -0.00540742 129.002701 Z M 172.005404 129.002701 " transform="matrix(0.288953,0,0,0.288953,0.15,0.15)"/>
                        <path d="M 0.148438 7.605469 L 0.148438 12.574219 L 49.851562 12.574219 L 49.851562 7.605469 Z M 0.148438 22.515625 L 0.148438 27.484375 L 49.851562 27.484375 L 49.851562 22.515625 Z M 0.148438 37.425781 L 0.148438 42.394531 L 49.851562 42.394531 L 49.851562 37.425781 Z M 0.148438 37.425781 "/>
                        </g>
                    </svg>
                </div> */}
                </div>
            </div>
        </div>
    )
};

export default HeaderLogo;

