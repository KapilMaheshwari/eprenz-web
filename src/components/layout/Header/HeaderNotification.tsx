import React from "react";
import { Link } from "react-router-dom";
import Dropdown from 'react-bootstrap/Dropdown';


const HeaderNotification: React.FC = () => {
    return(
        <>
            <li>
                <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic" className="notification-toggle">
                        <i className="fa fa-bell"></i>
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <div className="header-popup-heading"> 
                            <p>Notifications</p>
                            <Link to="">See All</Link>
                        </div>
                        <div className="notification">
                            <div className="notification-main">
                                <h5>Today</h5>
                                <div className="notification-content">
                                    <div className="notificaion-detail">
                                        <i className="fa fa-user"></i>
                                        <p><span>Jess Hennesy</span> added new post</p>
                                    </div>
                                    <i className="fa fa-times-circle"></i>
                                </div>
                                <div className="notification-content">
                                    <div className="notificaion-detail">
                                        <i className="fa fa-user"></i>
                                        <p><span>Jess Hennesy</span> added new post</p>
                                    </div>
                                    <i className="fa fa-times-circle"></i>
                                </div>
                                <div className="notification-content">
                                    <div className="notificaion-detail">
                                        <i className="fa fa-user"></i>
                                        <p><span>Jess Hennesy</span> added new post</p>
                                    </div>
                                    <i className="fa fa-times-circle"></i>
                                </div>
                            </div>
                            <div className="notification-main">
                                <h5>Tommorow</h5>
                                <div className="notification-content">
                                    <div className="notificaion-detail">
                                        <i className="fa fa-user"></i>
                                        <p><span>Jess Hennesy</span> added new post</p>
                                    </div>
                                    <i className="fa fa-times-circle"></i>
                                </div>
                                <div className="notification-content">
                                    <div className="notificaion-detail">
                                        <i className="fa fa-user"></i>
                                        <p><span>Jess Hennesy</span> added new post</p>
                                    </div>
                                    <i className="fa fa-times-circle"></i>
                                </div>
                                <div className="notification-content">
                                    <div className="notificaion-detail">
                                        <i className="fa fa-user"></i>
                                        <p><span>Jess Hennesy</span> added new post</p>
                                    </div>
                                    <i className="fa fa-times-circle"></i>
                                </div>
                            </div>
                        </div>
                    </Dropdown.Menu>
                </Dropdown>
            </li>
        </>
    );
}

export default HeaderNotification;