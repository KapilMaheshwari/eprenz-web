import React from "react";
import { Link } from "react-router-dom";
import user1 from './../../../assets/images/user/user1.jpg';

const HeaderUserRequest: React.FC = () => {
    return(
        <>
            <li>
                <a href="#" className="search-toggle pr-0 ak-header-chat-icon d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" xlinkHref="http://www.w3.org/1999/xlink" width="25" height="25" viewBox="0 0 25 25">
                        <image id="Vector_Smart_Object" data-name="Vector Smart Object" width="25" height="25" xlinkHref="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAACzElEQVRIiaWWWYiNYRjHf5asWS7sOciFmjmTJVuUKMoyxOCp0ZAoipK5EeVCEUlJSC7kQtyMP1niQjLZS8mF5bNcIIWSfWwxjJ7pmRrH+cZx5qnT+513+z3797UhRcysHbAcWAGMAPz/I+AwsEfS57SzuZIXYmZdgFPANOATcA34BkwEegN3gemSXhQCaZsyvy8ArnVG0gxJ84CBwFagDDhuZmnn/5B2eawoBQ4AF4H5kr42rSVJ8jNJktpsNtsfcOjtJEnuF2PJrBi3S/qVcm5bjHMKsSQfJNOkeNohSc8iVpm0Pf+CfIyxT9ohM+sGdAXeFwu5HOOyFs4ticy8VCzkAnALWGVmVbmLZjYJ2AG8Bg4VAkmrk2HAlXDZOeAMUA9M9mXgOzBbUm1REDPrBPQKK3cDc3P2ObwaeAO8lVRXMCS0dzeUA+2B/cAGoCMwKqAPgA/ALmAp0ABc9X2SrrcIMbMxEYvuMfYAfO4tcBK4E5DhQEXs80tfudvirgmSbuaDtDWzDsDR0HimJG8nE4A1wLtokq75ztD+ObASmCSpIpQ5EvP5LTGzhV5fwCZJm/PEaAjgP6/+p1GIzdd7RozczQMi6867UpJeNkE8DuuAUkn/7EM5AI/VWaB/JMLj6AL9InYLJF1wP/eNM6nmpgB6BsAzcbVfLGlcWFMZ8T5hZkMd4psbJH0s4O7mUh0WrJXkmTjYzLzfjZdUA3ghe/vZ2AQpqAflSHm46IBrCywCSvwVYGYlkryA7/m+1kDcLY8l1UdH2BLz64Eb8fzQw9EaiGdRxszaRJrvjfmD8UJzGeRl4NnlWfAjKrdQ8fqpi1qq9BiYWVkU6DzvaWY2Mb4NahzSUIQVX4ApUQ9uSVXEoFECcCwyb3TqJ1EhYmZTPU0jizzIHgN3kXcB984St/KvD4n/kSRJnmSzWU9Xh4wExgKdgdPA4sZXAfAbHj/rGwLgbXoAAAAASUVORK5CYII="/>
                    </svg>
                </a>
                <div className="iq-sub-dropdown iq-user-dropdown request-popup">
                    <div className="iq-card shadow-none m-0">
                        <div className="iq-card-body p-0 ak-notification-main">
                            <div className="py-2 px-3">
                                <div className="row header-popup-heading"> 
                                <p>Connections Requests</p>
                                <Link to="">See All</Link>
                                </div>
                            </div>
                            <hr className="m-0 p-0" />
                            <div className="ak-notification-content">
                                <a href="#">
                                <div className="media ak-media-main">
                                    <div className="d-flex align-items-center">
                                        <img src={user1} className="mr-3" alt="..."/>
                                        <div className="media-body">
                                            <h6 className="mt-0">Lesley Schwartz</h6>
                                            <p>Tesla Inc.</p>
                                        </div>
                                    </div>
                                    <div className="request-popup-btn">
                                        <button type="button" className="accept-btn">ACCEPT</button>
                                        <button type="button" className="ignore-btn">IGNORE</button>
                                    </div>
                                </div>
                                </a>
                                <a href="#">
                                <div className="media ak-media-main">
                                    <div className="d-flex align-items-center">
                                        <img src={user1} className="mr-3" alt="..."/>
                                        <div className="media-body">
                                            <h6 className="mt-0">Lesley Schwartz</h6>
                                            <p>Tesla Inc.</p>
                                        </div>
                                    </div>
                                    <div className="request-popup-btn">
                                        <button type="button" className="accept-btn">ACCEPT</button>
                                        <button type="button" className="ignore-btn">IGNORE</button>
                                    </div>
                                </div>
                                </a>
                                <a href="#">
                                <div className="media ak-media-main">
                                    <div className="d-flex align-items-center">
                                        <img src={user1} className="mr-3" alt="..."/>
                                        <div className="media-body">
                                            <h6 className="mt-0">Lesley Schwartz</h6>
                                            <p>Tesla Inc.</p>
                                        </div>
                                    </div>
                                    <div className="request-popup-btn">
                                        <button type="button" className="accept-btn">ACCEPT</button>
                                        <button type="button" className="ignore-btn">IGNORE</button>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </>
    );
}

export default HeaderUserRequest;