import React from "react";
import { Link } from "react-router-dom";

const HeaderMenu: React.FC = () => {
    return(
        <>
            <ul className="navbar-list ak-menubar">
                <li>
                    <a href="#" className="search-toggle   d-flex align-items-center">
                    <i className="ri-arrow-down-s-fill"></i>
                    </a>
                    <div className="iq-sub-dropdown iq-user-dropdown">
                    <div className="iq-card shadow-none m-0">
                        <div className="iq-card-body">
                            <Link to="/profile" className="iq-sub-card iq-bg-primary-hover">
                                <div className="media align-items-center">
                                <h6 className="mb-0 ">My Profile</h6>
                                </div>
                            </Link>
                            <Link to="/profile" className="iq-sub-card iq-bg-primary-hover">
                                <div className="media align-items-center">
                                <h6 className="mb-0 ">Connections</h6>
                                </div>
                            </Link>
                            <Link to="/event-history" className="iq-sub-card iq-bg-primary-hover">
                                <div className="media align-items-center">
                                <h6 className="mb-0 ">Events</h6>
                                </div>
                            </Link>
                            <Link to="/clubs" className="iq-sub-card iq-bg-primary-hover">
                                <div className="media align-items-center">
                                <h6 className="mb-0 ">Clubs</h6>
                                </div>
                            </Link>
                            <Link to="/profile/settings" className="iq-sub-card iq-bg-primary-hover">
                                <div className="media align-items-center">
                                <h6 className="mb-0 ">Account Settings</h6>
                                </div>
                            </Link>
                            <Link to="/sign-in" className="iq-sub-card iq-bg-primary-hover">
                                <div className="media align-items-center">
                                <h6 className="mb-0 ">Logout</h6>
                                </div>
                            </Link>
                        </div>
                    </div>
                    </div>
                </li>
            </ul>
        </>
    );
}

export default HeaderMenu;
