import React from "react";
import { NavLink } from "react-router-dom";
import './SidebarStyle.scss';

const Sidebar: React.FC = () => {
    return(
        <div className="iq-sidebar">
            <div id="sidebar-scrollbar" style={{overflow: "hidden", outline: "none",}}>
                <nav className="iq-sidebar-menu">
                    <ul id="iq-sidebar-toggle" className="iq-menu">
                    <li>
                        <NavLink to="/">
                            <svg className="sidebar-icon" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="20px" height="22.01px" viewBox="0 0 20 22.01" enableBackground="new 0 0 20 22.01" xlinkHref="preserve">
                                <g><path fill="none" stroke="#819BA2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" d="
                                M1,8.01L10,1l9,7.01v10.83c0,1.1-0.9,2-2,2H3c-1.1,0-2-0.9-2-2V8.01z"/><polyline fill="none" stroke="#819BA2" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="10" points="
                                7,21.01 7,10.84 13,10.84 13,21.01"/></g>
                            </svg>
                            <span className="home-menu">Home</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/discovery">
                            <svg className="sidebar-fill" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="18px" height="22px" viewBox="0 0 18 22" enableBackground="new 0 0 18 22" xlinkHref="preserve">
                                <g><g><path fill="#819BA2" d="M1,19.5c-0.552,0-1-0.448-1-1C0,16.57,1.57,15,3.5,15h12.757c0.183-0.203,0.448-0.33,0.743-0.33
                                c0.552,0,1,0.448,1,1V16c0,0.552-0.448,1-1,1H3.5C2.673,17,2,17.673,2,18.5C2,19.052,1.552,19.5,1,19.5z"/>
                                </g><g><path fill="#819BA2" d="M17,22H3.5C1.57,22,0,20.43,0,18.5v-15C0,1.57,1.57,0,3.5,0H17c0.552,0,1,0.448,1,1v20
                                C18,21.552,17.552,22,17,22z M3.5,2C2.673,2,2,2.673,2,3.5v15C2,19.327,2.673,20,3.5,20H16V2H3.5z"/></g></g>
                            </svg>
                            <span className="home-menu">Discovery</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/topics">
                            <svg className="sidebar-fill" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="18px" height="22.34px" viewBox="0 0 18 22.34" enableBackground="new 0 0 18 22.34" xlinkHref="preserve">
                                <g><g><path fill="#819BA2" d="M15,22.34H3c-1.654,0-3-1.346-3-3V3c0-1.566,1.233-2.66,3-2.66h8c0.265,0,0.52,0.105,0.707,0.293l6,6
                                C17.895,6.82,18,7.075,18,7.34v12C18,20.994,16.654,22.34,15,22.34z M3,2.34C2.626,2.34,2,2.426,2,3v16.34c0,0.551,0.449,1,1,1h12
                                c0.551,0,1-0.449,1-1V7.754L10.586,2.34H3z"/></g><g><path fill="#819BA2" d="M17,8.34h-6c-0.552,0-1-0.448-1-1V1c0-0.552,0.448-1,1-1s1,0.448,1,1v5.34h4.257
                                C16.44,6.137,16.706,6.01,17,6.01c0.552,0,1,0.448,1,1v0.33C18,7.892,17.552,8.34,17,8.34z"/>
                                </g><g><path fill="#819BA2" d="M13,13.34H5c-0.552,0-1-0.448-1-1v-0.33c0-0.552,0.448-1,1-1c0.294,0,0.56,0.127,0.743,0.33H13
                                c0.552,0,1,0.448,1,1S13.552,13.34,13,13.34z"/></g><g><path fill="#819BA2" d="M13,17.34H5c-0.552,0-1-0.448-1-1v-0.33c0-0.552,0.448-1,1-1c0.294,0,0.56,0.127,0.743,0.33H13
                                c0.552,0,1,0.448,1,1S13.552,17.34,13,17.34z"/></g><g><path fill="#819BA2" d="M7,9.34H5c-0.552,0-1-0.448-1-1V8.01c0-0.552,0.448-1,1-1c0.294,0,0.56,0.127,0.743,0.33H7
                                c0.552,0,1,0.448,1,1S7.552,9.34,7,9.34z"/></g></g>
                            </svg>
                            <span className="home-menu">Topics</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/members">
                            <svg className="sidebar-fill" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="24px" height="20.01px" viewBox="0 0 24 20.01" enableBackground="new 0 0 24 20.01" xlinkHref="preserve">
                                <g><g><path fill="#819BA2" d="M17,20.01c-0.552,0-1-0.448-1-1v-2c0-1.654-1.346-3-3-3H5c-1.654,0-3,1.346-3,3v2c0,0.552-0.448,1-1,1
                                s-1-0.448-1-1v-2c0-2.757,2.243-5,5-5h8c2.757,0,5,2.243,5,5v2C18,19.562,17.552,20.01,17,20.01z"/>
                                </g><g><path fill="#819BA2" d="M9,10.01c-2.757,0-5-2.248-5-5.01c0-2.757,2.243-5,5-5s5,2.243,5,5C14,7.762,11.757,10.01,9,10.01z M9,2
                                C7.346,2,6,3.346,6,5c0,1.66,1.346,3.01,3,3.01S12,6.66,12,5C12,3.346,10.654,2,9,2z"/>
                                </g><g><path fill="#819BA2" d="M23,20.01c-0.552,0-1-0.448-1-1v-2c0-1.37-0.924-2.562-2.248-2.901c-0.535-0.137-0.857-0.682-0.721-1.217
                                s0.679-0.859,1.217-0.721C22.457,12.736,24,14.726,24,17.01v2C24,19.562,23.552,20.01,23,20.01z"/>
                                </g><g><path fill="#819BA2" d="M16,9.89c-0.444,0-0.85-0.298-0.967-0.748c-0.139-0.535,0.181-1.081,0.716-1.22
                                c1.065-0.277,1.894-1.108,2.163-2.167c0.2-0.78,0.085-1.587-0.323-2.277c-0.408-0.69-1.06-1.18-1.837-1.379
                                c-0.535-0.138-0.857-0.683-0.72-1.218c0.138-0.534,0.682-0.857,1.218-0.72c1.293,0.333,2.38,1.149,3.061,2.298
                                c0.68,1.15,0.872,2.496,0.539,3.79c-0.447,1.763-1.825,3.147-3.596,3.608C16.167,9.879,16.083,9.89,16,9.89z"/>
                                </g></g>
                            </svg>
                            <span className="home-menu">Members</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/events">
                            <svg className="sidebar-fill" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="20px" height="21.84px" viewBox="0 0 20 21.84" enableBackground="new 0 0 20 21.84" xlinkHref="preserve">
                                <g><g><path fill="#819BA2" d="M17,21.84H3c-1.654,0-3-1.346-3-3v-14c0-1.654,1.346-3,3-3h14c1.626,0,3,1.447,3,3.16v13.84
                                C20,20.494,18.654,21.84,17,21.84z M3,3.84c-0.551,0-1,0.449-1,1v14c0,0.551,0.449,1,1,1h14c0.551,0,1-0.449,1-1V5
                                c0-0.585-0.495-1.16-1-1.16H3z"/></g><g><path fill="#819BA2" d="M14,6c-0.552,0-1-0.448-1-1V1c0-0.552,0.448-1,1-1s1,0.448,1,1v4C15,5.552,14.552,6,14,6z"/>
                                </g><g><path fill="#819BA2" d="M6,6C5.448,6,5,5.552,5,5V1c0-0.552,0.448-1,1-1s1,0.448,1,1v4C7,5.552,6.552,6,6,6z"/>
                                </g><g><path fill="#819BA2" d="M19,10.01c-0.207,0-0.398-0.062-0.558-0.17H1c-0.552,0-1-0.448-1-1s0.448-1,1-1h18c0.552,0,1,0.448,1,1
                                v0.17C20,9.562,19.552,10.01,19,10.01z"/></g></g>
                            </svg>
                            <span className="home-menu">Events</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/clubs">
                            <svg className="sidebar-fill" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            width="22px" height="22.01px" viewBox="0 0 22 22.01" enableBackground="new 0 0 22 22.01" xlinkHref="preserve">
                            <g><g><path fill="#819BA2" d="M11,22.01c-6.065,0-11-4.935-11-11C0,4.939,4.935,0,11,0s11,4.939,11,11.01
                            C22,17.075,17.065,22.01,11,22.01z M11,2c-4.962,0-9,4.042-9,9.01c0,4.962,4.038,9,9,9s9-4.038,9-9C20,6.042,15.962,2,11,2z"/>
                            </g><g><path fill="#819BA2" d="M21,12.01H1c-0.552,0-1-0.448-1-1s0.448-1,1-1h20c0.552,0,1,0.448,1,1S21.552,12.01,21,12.01z"/>
                            </g><g><path fill="#819BA2" d="M11,22.01c-0.281,0-0.549-0.118-0.739-0.326C7.599,18.766,6.085,14.983,6,11.031
                            C6.085,7.03,7.599,3.244,10.261,0.326c0.379-0.416,1.099-0.416,1.478,0C14.401,3.244,15.915,7.03,16,10.988
                            c-0.085,3.995-1.599,7.778-4.261,10.695C11.549,21.892,11.281,22.01,11,22.01z M11,2.552c-1.876,2.42-2.934,5.39-3,8.479
                            c0.066,3.045,1.124,6.008,3,8.427c1.876-2.419,2.934-5.386,3-8.47C13.934,7.937,12.876,4.972,11,2.552z"/>
                            </g></g>
                            </svg>
                            <span className="home-menu">Clubs</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/companies">
                            <svg className="sidebar-fill" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            width="22px" height="20px" viewBox="0 0 22 20" enableBackground="new 0 0 22 20" xlinkHref="preserve">
                            <g><g><path fill="#819BA2" d="M19,20H3c-1.654,0-3-1.346-3-3V7c0-1.654,1.346-3,3-3h16c1.767,0,3,1.098,3,2.67V17
                            C22,18.654,20.654,20,19,20z M3,6C2.449,6,2,6.449,2,7v10c0,0.551,0.449,1,1,1h16c0.551,0,1-0.449,1-1V6.67
                            C20,6.116,19.456,6,19,6H3z"/></g><g>
                            <path fill="#819BA2" d="M15,19.67c-0.552,0-1-0.448-1-1V3c0-0.551-0.449-1-1-1H9C8.449,2,8,2.449,8,3v15.67c0,0.552-0.448,1-1,1
                            s-1-0.448-1-1V3c0-1.654,1.346-3,3-3h4c1.654,0,3,1.346,3,3v15.67C16,19.222,15.552,19.67,15,19.67z"/>
                            </g></g></svg>
                            <span className="home-menu">Companies</span>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/deals">
                            <svg className="sidebar-fill" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                width="21.684px" height="22.34px" viewBox="0 0 21.684 22.34" enableBackground="new 0 0 21.684 22.34" xlinkHref="preserve">
                                <g><g><path fill="#819BA2" d="M17.301,22.34c-0.01,0-0.021,0-0.031,0H3c-1.717,0-3-1.758-3-3.33v-6.67c0-1.654,1.346-3,3-3h2.203
                                l3.883-8.746C9.247,0.233,9.604,0,10,0c2.206,0,4,1.794,4,4v3.34h4.66c0.145,0,0.29,0.012,0.422,0.025
                                c1.799,0.279,2.81,1.521,2.566,3.098l-1.379,9.323C20.044,21.271,18.798,22.34,17.301,22.34z M7,20.34h10.28
                                c0.518,0.01,0.936-0.353,1.011-0.85l1.38-9.326c0.026-0.17,0.103-0.668-0.845-0.815c-0.016,0-0.091-0.008-0.166-0.008H13
                                c-0.552,0-1-0.448-1-1V4c0-0.891-0.585-1.647-1.392-1.905L7,10.34V20.34z M3,11.34c-0.551,0-1,0.449-1,1v6.67
                                c0,0.622,0.533,1.33,1,1.33h2v-9H3z"/></g></g>
                            </svg>
                            <span className="home-menu">Deals</span>
                        </NavLink>
                    </li>
                    </ul>
                </nav>
            </div>
        </div>
    ); 
}

export default Sidebar; 