
import { Button, Modal } from 'react-bootstrap';

export interface Props {
    children: any,
    setShowModal: any,
    showModal: any
}

const ModalComponent = ({
    children,
    setShowModal,
    showModal
}: Props) => {
    return (
        <Modal show={showModal} onHide={() => setShowModal(false)}>
            <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={() => setShowModal(false)}>
                Close
            </Button>
            <Button variant="primary" onClick={() => setShowModal(false)}>
                Save Changes
            </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ModalComponent;
