import styled from "@emotion/styled";
import Card from "../Card/Card";
import CardBody from "../Card/CardBody";
import Location from "../Location/Location";
import User from "../../assets/images/user/user1.jpg"

export interface Props {
    data: any
}

const MembersList = ({ data }: any) =>(
    <ListView>
        <Card>
            <CardBody>
                <img src={User} alt="users"/>
                <h5>{data.name}</h5>
                <h6><Location />{data.location}</h6>
                <p>{data.company}</p>
                <span></span>
            </CardBody>
        </Card>
    </ListView>
);

const ListView = styled.div`
    text-align: center;
    margin-bottom: 10px;
    img{
        width: 70px;
        height: 70px;
        object-fit: cover;
        border-radius: 50px;
        margin-bottom: 8px;
    }
    h5{
        font-size: 15px;
        font-weight: 700;
        letter-spacing: 0.3px;
        color: #002c3e;
        margin-bottom: 3px;
    }
    h6{
        font-size: 14px;
        color: #555;
        font-weight: 600;
        letter-spacing: 0.5px;
        margin-bottom: 3px;
    }
    p{
        color: #3aabef;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: 0.5px;
    }
`;

export default MembersList;